const express = require('express');
const { signup, login, getLocation, storeLocation} = require('../controller/userController');
const auth = require('../middleware/auth');
const router = express.Router();


router.post('/signup' , signup);
router.post('/login', login);
router.get('/geolocation/:address' ,auth, getLocation);
router.post('/store-geolocation/:lat/:long' ,storeLocation)




module.exports = router