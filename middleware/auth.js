const jwt = require('jsonwebtoken')
const Secret_key = "SOMETHING";

const auth = async (req, res, next) => {

    try {
        let token = req.headers.authorization;
      
        if (token) {
            token = token.split(" ")[1];
            let user = jwt.verify(token, Secret_key);
            //  console.log(user);
            req.body = {userId:user.id};
        }else{
            return res.status(401).json({ message: "unauthorized user" })
        }
        next();
    } catch (error) {
        console.log(error);
        res.status(401).json({ message: "unauthorized user!!!" });
    }
}

module.exports = auth