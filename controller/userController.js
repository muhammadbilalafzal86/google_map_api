const userModel = require('../model/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Secret_key = "SOMETHING";
const Joi = require('joi');
const axios = require('axios');
require('dotenv').config();
const Airtable = require('airtable');




const signup = async (req, res) => {
    const { name, email, password,} = req.body;

    try {
        const {error} = userValidation(req.body);
        if (error) {
            res.status(400).send(error.details[0].message);
            return;
        }

        const existingUser = await userModel.findOne({ email: email });
        if (existingUser) {
            return res.status(400).json({ message: "user already exist" });

        }
        const hashedPassword = await bcrypt.hash(password, 10);
        const result = await userModel.create({
            ...req.body,
            password: hashedPassword
        });
        res.status(201).json({ msg: "Account is successfully created" });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "something goes wrong"
        });
    }


}

const login = async (req, res) => {
    const { email, password } = req.body;
    try {

        const existingUser = await userModel.findOne({ email: email });
        if (!existingUser) {
            return res.status(404).json({ message: "user not found" });

        }
        const passwordMatch = await bcrypt.compare(password, existingUser.password);
        if (!passwordMatch) return res.status(400).json({ message: "invalid Credentials" });

        const token = jwt.sign({email:existingUser.email , id: existingUser._id}, Secret_key)
        res.status(200).json({ msg: "login successfully", token: token });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            message: "something goes wrong"
        });
    }

}

const getLocation =async(req,res)=>{
    // try {
    //     const id = req.body.userId;
    //     const profile = await userModel.findById(id);
    //     res.status(200).json({ profile });
    // } catch (error) {
    //     console.log(error);
    //     res.status(400).json({ message: "something goes wrong" })
    // }
    
        try {
          const address = req.params.address;
          if (!address) {
            return res.status(400).json({ error: 'Address parameter is missing' });
          }
          const encodedAddress = encodeURIComponent(address);

        //   console.log(encodedAddress);
          const apiKey = 'AIzaSyAmV6ikDrLuSnwniMD-x-IGBYIXZBcsIoM';
          const geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${apiKey}`;
      
          const response = await axios.get(geocodeUrl);
        //   console.log(response.data.results[0].geometry.location);
          const data = response.data;
      
          if (data.status === 'OK') {
            const location = data.results[0].geometry.location;
            res.status(200).json({ location });
          } else {
            res.status(400).json({ error: 'Geolocation data not found' });
          }
        } catch (error) {
          console.error('Error retrieving geolocation:', error);
          res.status(500).json({ error: 'Failed to retrieve geolocation' });
        }
      
      
}

const storeLocation =async(req,res)=>{
    

        try {
          
          const lat= req.params.lat;
          const long = req.params.long;
          if (!lat || !long) {
            return res.status(400).json({ error: 'Latitude or longitude is missing' });
          }
      
          const base = new Airtable({ apiKey: process.env.AIRTABLE_API_KEY }).base(process.env.AIRTABLE_BASE_ID);
          const table = base('map-api');
      
          const record = await table.create([
            {
              fields: {
                Latitude: lat,
                Longitude: long
              }
            }
          ]);
      
        //   res.status(200).json({ recordId: record[0].getId() });
        res.status(200).json({ msg:"record created successfully" });
        } catch (error) {
          console.error('Error storing geolocation:', error);
          res.status(500).json({ error: 'Failed to store geolocation' });
        }

      
    
      
}

function userValidation(user) {
    const schema = Joi.object({
        name: Joi.string().min(3).max(50).required().pattern(/^[a-zA-Z\s]+$/).messages({
            'string.base': 'Name must be a string',
            'string.empty': 'Name is required',
            'string.min': 'Name should have a minimum length of {#limit}',
            'string.max': 'Name should have a maximum length of {#limit}',
            'string.pattern.base': 'Name can only contain letters and spaces',
            'any.required': 'Name is required',
        }),
        email: Joi.string().min(6).max(255).required().email(),
        password: Joi.string().min(5).max(10).required().regex(/^(?=.*[A-Z]).*$/).messages({
            'string.base': 'Password must be a string',
            'string.empty': 'Password is required',
            'string.min': 'Password should have a minimum length of {#limit}',
            'string.pattern.base': 'Password must contain at least one uppercase letter',
            'any.required': 'Password is required',
        }),
    })
    return schema.validate(user);
}

module.exports = {signup , login , getLocation, storeLocation}