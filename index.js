const express = require('express')
const router = require('./router/userRouts');
const mongoose = require('mongoose');


const app = express();
app.use(express.json());

app.use('/api/users' , router)

mongoose.connect('mongodb://127.0.0.1/Map-Task')
.then(()=>{
    app.listen(3001 , ()=>{
        console.log('server run on port 3001');
    });
})
.catch((err)=>{
    console.log(err);
})
